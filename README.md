yumrepo
=========

This role configures yum repositories hosted on a local cdnmirror. 

Requirements
------------

RPM content hosted somewhere using the cdnmirror role.

Role Variables
--------------

| variable    | default            | description                           |
| ----------- | ------------------ | ------------------------------------- |
| yum_repos   | rhel-7-server-rpms | a list of yum repositories            |
| repo_server | null               | the server where the rpms are located |



Example Playbook
----------------

    - hosts: servers
      vars:
        yum_repos:
          - rhel-7-server-rpms
          - rhel-7-server-extras-rpms
      roles:
         - yumrepo

License
-------

BSD

